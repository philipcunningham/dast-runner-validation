# frozen_string_literal: true

require 'concurrent-edge'
require 'sinatra'

class WebApp < Sinatra::Base
  MAX_MESSAGES = 50
  MESSAGES = Concurrent::Channel.new(capacity: MAX_MESSAGES)

  def self.take_all_messages
    MESSAGES.length.times.map { MESSAGES.take }
  end

  post '/internal/dast/site_validations/:id/transition' do
    message = { id: Integer(params['id']), event: JSON.parse(request.body.read)['event'], token: request.env['HTTP_JOB_TOKEN'] }

    Concurrent::Channel.go { MESSAGES.put(message) }
  end

  get '/400_error' do
    status 400
    body 'This is a 400 error'
  end

  get '/500_error' do
    status 500
    body 'This is a 500 error'
  end

  get '/json' do
    content_type :json

    'validation-token'
  end

  get '/header' do
    response.headers['Validation-Header'] = 'validation-token'
  end

  get '/text_file' do
    response.headers['Content-Type'] = 'text/file'

    'validation-token'
  end

  get '/redirect/:strategy' do
    redirect "/#{params[:strategy]}"
  end

  eventually_header_counter = 0
  get '/eventually_header' do
    eventually_header_counter += 1

    if eventually_header_counter > 2
      eventually_header_counter = 0

      response.headers['Validation-Header'] = 'validation-token'
    end
  end

  eventually_text_file_counter = 0
  get '/eventually_text_file' do
    eventually_text_file_counter += 1

    response.headers['Content-Type'] = 'text/file'

    if eventually_text_file_counter > 2
      eventually_text_file_counter = 0

      'validation-token'
    end
  end
end
