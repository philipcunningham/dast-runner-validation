#!/usr/bin/env bash

set -Eeuo pipefail

function error {
    echo "$1" >&2
    exit 1
}

function must_not_be_empty {
    error "$1: must not be empty"
}

[[ -z "$CI_COMMIT_SHA" ]]        && must_not_be_empty "CI_COMMIT_SHA"
[[ -z "$CI_PROJECT_ID" ]]        && must_not_be_empty "CI_PROJECT_ID"
[[ -z "$CI_REGISTRY" ]]          && must_not_be_empty "CI_REGISTRY"
[[ -z "$CI_REGISTRY_IMAGE" ]]    && must_not_be_empty "CI_REGISTRY_IMAGE"
[[ -z "$CI_REGISTRY_PASSWORD" ]] && must_not_be_empty "CI_REGISTRY_PASSWORD"
[[ -z "$CI_REGISTRY_USER" ]]     && must_not_be_empty "CI_REGISTRY_USER"
[[ -z "$GITLAB_PRIVATE_TOKEN" ]] && must_not_be_empty "GITLAB_PRIVATE_TOKEN"
[[ -z "$SOURCE_DOCKER_IMAGE" ]]  && must_not_be_empty "SOURCE_DOCKER_IMAGE"
[[ -z "$VERSION" ]]              && must_not_be_empty "VERSION"

tag_and_push() {
    docker tag "$SOURCE_DOCKER_IMAGE" "$1"
    docker push "$1"
}

BASE_URL="https://gitlab.com/api/v4/projects/$CI_PROJECT_ID"
TAG_NAME="v$VERSION"

echo "Checking tag"
if curl --silent --fail --show-error --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" "$BASE_URL/repository/tags/$TAG_NAME"; then
    error "Aborting, tag $TAG_NAME already exists. If this is not expected, please remove the tag and try again."
fi

echo "Releasing Docker images"
docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
docker pull "$SOURCE_DOCKER_IMAGE"
tag_and_push "$CI_REGISTRY_IMAGE:$VERSION"
tag_and_push "$CI_REGISTRY_IMAGE:latest"

echo "Creating tag: $TAG_NAME"
curl --fail \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --request POST "$BASE_URL/repository/tags?tag_name=$TAG_NAME&ref=$CI_COMMIT_SHA"

echo "Updating changelog"
curl --fail \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --data "version=$VERSION&to=$CI_COMMIT_SHA" \
     "$BASE_URL/repository/changelog"

echo "Creating release"
curl --fail \
     --header "PRIVATE-TOKEN: $GITLAB_PRIVATE_TOKEN" \
     --header "Content-Type: application/json" \
     --data "{ \"tag_name\": \"v$VERSION\", \"description\": \"See CHANGELOG.md\" }" \
     --request POST \
     "$BASE_URL/releases"
