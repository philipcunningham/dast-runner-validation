## 1.0.0 (2021-07-06)

### added (1 change)

- [Add smoke test to .gitlab-ci.yml](philipcunningham/dast-runner-validation@baa8b7bcbf1d4bee7b4e27d33f76e8c7f258fb20)

### feature (5 changes)

- [Push intermediate images to container registry](philipcunningham/dast-runner-validation@1ade0cd958b1b25b4c387dab2fb97741d8601789)
- [Add files outlined by new project guidelines](philipcunningham/dast-runner-validation@b08f1448123379b598b2c19ff2b82f984220d867)
- [Add security checks to CI config](philipcunningham/dast-runner-validation@f6e602fc0150a202f04fc1a12d2a0fdf9e07ff0f)
- [Add script for performing DAST site validation](philipcunningham/dast-runner-validation@654be7fb8d4f3e722bb3a7b2f2819058eb304825)
- [Add LICENSE as per handbook guidelines](philipcunningham/dast-runner-validation@6c67a9ed7abb8398cb7fcf7340ee4283c2c94009)
