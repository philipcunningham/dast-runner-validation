#!/usr/bin/env bash

set -Eeuo pipefail

function must_not_be_empty {
    echo "$1: must not be empty" >&2
    exit 1
}

[[ -z "$CI_JOB_TOKEN" ]]                  && must_not_be_empty "CI_JOB_TOKEN"
[[ -z "$CI_SERVER_URL" ]]                 && must_not_be_empty "CI_SERVER_URL"
[[ -z "$DAST_SITE_VALIDATION_ID" ]]       && must_not_be_empty "DAST_SITE_VALIDATION_ID"
[[ -z "$DAST_SITE_VALIDATION_HEADER" ]]   && must_not_be_empty "DAST_SITE_VALIDATION_HEADER"
[[ -z "$DAST_SITE_VALIDATION_STRATEGY" ]] && must_not_be_empty "DAST_SITE_VALIDATION_STRATEGY"
[[ -z "$DAST_SITE_VALIDATION_TOKEN" ]]    && must_not_be_empty "DAST_SITE_VALIDATION_TOKEN"
[[ -z "$DAST_SITE_VALIDATION_URL" ]]      && must_not_be_empty "DAST_SITE_VALIDATION_URL"

function call_gitlab {
    local url="$CI_SERVER_URL/internal/dast/site_validations/$DAST_SITE_VALIDATION_ID/transition"

    curl --fail --request POST --data "{\"event\":\"$1\"}" --header "JOB-TOKEN: $CI_JOB_TOKEN" "$url" --header "Content-Type: application/json"
}

function retry {
    local n=1
    local max="${MAX_RETRY:-5}"
    local delay="${MAX_RETRY_DELAY:-15}"

    while true; do
        # shellcheck disable=SC2015
        "$@" && break || {
                if [[ $n -le $max ]]; then
                    ((n++))
                    echo "Validation failed: Retrying"
                    sleep "$delay"
                    call_gitlab "retry"
                else
                    echo "Validation failed: Retry limit exceeded"
                    call_gitlab "fail_op"

                    exit 1
                fi
            }
    done
}

function validation_curl {
    curl --location --insecure --include --dump-header "$@"
}

function header_validation {
    local tmp_file
    local response

    tmp_file=$(mktemp)
    response=$(validation_curl "$tmp_file" "$DAST_SITE_VALIDATION_URL")

    echo "$response" | head -n 1

    grep -iq "$DAST_SITE_VALIDATION_HEADER: $DAST_SITE_VALIDATION_TOKEN" "$tmp_file"
}

function text_file_validation {
    local tmp_file
    local response

    tmp_file=$(mktemp)
    response=$(validation_curl "$tmp_file" "$DAST_SITE_VALIDATION_URL")

    echo "$response" | head -n 1

    if ! grep -iq "content-type: text/file" "$tmp_file";
    then
        echo "Validation failed: Expected content-type to be text/file"
        return 1
    else
        echo "$response" | grep -q "$DAST_SITE_VALIDATION_TOKEN"
    fi
}

function validate {
    case "$DAST_SITE_VALIDATION_STRATEGY" in

        header)
            header_validation
            ;;

        text_file)
            text_file_validation
            ;;

        *)
            echo "Validation failed: Invalid validation strategy"
            call_gitlab "fail_op"
            exit 1
            ;;
    esac
}

call_gitlab "start"

retry validate

call_gitlab "pass"

echo 'Validation success'
